<?php
namespace AIFap\Indexer\Data;

use AIFap\Indexer\Data\TypeHints;

/**
 * Represents the indexer configuration for processing a given data source.
 */
class SourceConfig {
    protected $sourceKey = null;
    protected $sourceInfo = [];
    protected $resumeInfo = [];
    protected $typeHints;
    
    public function __construct($key = null, TypeHints $typeHints) {
        $this->sourceKey = $key;
        $this->typeHints = $typeHints;
    }
    
    public function getTypeHints() {
        return $this->typeHints;
    }
    
    public function setSourceInfo($key, $value) {
        $this->sourceInfo[$key] = $value;
    }
    
    public function getSourceInfo($key, $options = []) {
        $options = array_merge([
            'required' => false,
            'default' => null,
        ], $options);
        
        if (isset($this->sourceInfo[$key])) {
            return $this->sourceInfo[$key];
        }
        if ($options['required']) {
            throw new \Exception("Source info '$key' is required.");
        }
        return $options['default'];
    }
    
    public function setResumeInfo($resumeInfo) {
        $this->resumeInfo = $resumeInfo;
    }
    
    public function getResumeInfo($key, $options = []) {
        $options = array_merge([
            'required' => false,
            'default' => null,
        ], $options);
        
        if (isset($this->resumeInfo[$key])) {
            return $this->resumeInfo[$key];
        }
        if ($options['required']) {
            throw new \Exception("Resume info '$key' is required.");
        }
        return $options['default'];
    }
    
    public function getSourceKey() {
        return $this->sourceKey;
    }
}