<?php
namespace AIFap\Indexer;

use AIFap\Indexer\Data\SourceConfig;

use AIFap\Indexer\Sources;
use AIFap\Indexer\Processing;

/**
 * Main service for indexing actions.
 */
class IndexingService {
    private static $sources = [
        Sources\RedditNewPostsSource::SOURCE_KEY => Sources\RedditNewPostsSource::class,
    ];
    
    private static $processing = [
        Processing\GfycatUrlFixer::class,
        Processing\TypeGuessing::class,
    ];
    
    public static function registerProcessor(Processing\PostProcessor $processor) {
        $this->processing[] = $processor;
    }
    
    public static function registerSource(Sources\Source $source) {
        $this->sources[$source::SOURCE_KEY] = $source;
    }
    
    public function index(SourceConfig $sourceConfig) {
        $klass = $this->getSourceClass($sourceConfig->getSourceKey());
        $source = new $klass($sourceConfig);
        $source->setup();
        
        // Index new post data from source
        $indexingResult = $source->run($sourceConfig);
        
        // Clean up post data
        $postDatas = $indexingResult->postData;
        foreach (static::$processing as $processingClass) {
            $process = new $processingClass($sourceConfig);
            $postDatas = $process->run($postDatas);
        }
        $indexingResult->postData = $postDatas;
        
        return $indexingResult;
    }
    
    private function getSourceClass($key) {
        if (!isset(static::$sources[$key])) {
            throw new \Exception("Source '$key' is not supported.");
        }
        return static::$sources[$key];
    }
        
}