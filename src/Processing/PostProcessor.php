<?php
namespace AIFap\Indexer\Processing;

use AIFap\Indexer\Data\SourceConfig;

/**
 * Base class for processing (i.e. cleaning up) posts after they're indexed.
 */
abstract class PostProcessor {
    protected $config = null;
    
    public function __construct(SourceConfig $sourceConfig) {
        $this->config = $sourceConfig;
    }
    
    public abstract function run(array $postDataArray);
}