
<?php
if (file_exists(dirname(__DIR__) . '/.env.test')) {
    (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
} else {
    echo "\nWARNING: create a .env.test file! (see .env.test.example)\n";
}

use PHPUnit\Framework\TestCase as TestCase;

use AIFap\Indexer\Data\TypeConstants;
use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\TypeHints;
use AIFap\Indexer\Data\SourceConfig;

use AIFap\Indexer\IndexingService;

final class RedditNewPostsSourceTest extends TestCase {
    public function testCanIndexFromScratch() {
        // TODO: improve testing
        $sourceConfig = new SourceConfig('reddit-new-posts', new TypeHints([
        ]));
        $sourceConfig->setSourceInfo('subreddit_slug', 'ass');
        $sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
        $sourceConfig->setSourceInfo('limit', 3);
        $sourceConfig->setSourceInfo('gfycat_client_id', getenv('GFYCAT_CLIENT_ID'));
        $sourceConfig->setSourceInfo('gfycat_client_secret', getenv('GFYCAT_CLIENT_SECRET'));
        
        $service = new IndexingService;
        $result = $service->index($sourceConfig);
        
        $this->assertNotNull($result);
    }
    
    public function testCanResumeIndex() {
        $sourceConfig = new SourceConfig('reddit-new-posts', new TypeHints([
        ]));
        $sourceConfig->setSourceInfo('subreddit_slug', 'ass');
        $sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
        $sourceConfig->setSourceInfo('limit', 3);
        $sourceConfig->setSourceInfo('gfycat_client_id', getenv('GFYCAT_CLIENT_ID'));
        $sourceConfig->setSourceInfo('gfycat_client_secret', getenv('GFYCAT_CLIENT_SECRET'));
        
        $sourceConfig->setResumeInfo([
            'after_posts' => [
                't3_ak44rr',
            ],
        ]);
        
        $service = new IndexingService;
        $result = $service->index($sourceConfig);
        
        $this->assertNotNull($result);
    }
}